package main

import (
	"log/slog"
	"test-apps/auth-app-grpc/sso/internal/config"
	"test-apps/auth-app-grpc/sso/internal/logger"
)

func main() {
	cfg := config.NewYAML()

	log := logger.New(cfg.LogLevel)

	log.Info("app started",
		slog.String("logger level", cfg.LogLevel),
		slog.String("storage path", cfg.StoragePath),
		slog.Any("token ttl", cfg.TokenTTL),
	)

}
