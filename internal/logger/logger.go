package logger

import (
	"log/slog"
	"os"
	"test-apps/auth-app-grpc/sso/internal/logger/slogpretty"
)

const (
	prod  = "prod"
	dev   = "dev"
	local = "local"
)

func New(logLevel string) *slog.Logger {
	var log *slog.Logger

	switch logLevel {
	case local:
		log = setupPrettySlog()
	case dev:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case prod:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
		)
	}

	return log
}

func setupPrettySlog() *slog.Logger {
	opts := slogpretty.PrettyHandlerOptions{
		SlogOpts: &slog.HandlerOptions{
			Level: slog.LevelDebug,
		},
	}

	handler := opts.NewPrettyHandler(os.Stdout)

	return slog.New(handler)
}

func Err(err error) slog.Attr {
	return slog.Attr{
		Key:   "error",
		Value: slog.StringValue(err.Error()),
	}
}
