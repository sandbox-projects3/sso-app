package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"os"
	"time"
)

type Config struct {
	LogLevel    string        `env:"LOG_LEVEL" yaml:"log_level" env-required:"true"`
	StoragePath string        `env:"STORAGE_PATH" yaml:"storage_path" env-required:"true"`
	TokenTTL    time.Duration `env:"TOKEN_TTL" yaml:"token_ttl" env-required:"true"`
	GRPC        GRPCConfig    `env:"GRPC" yaml:"grpc"`
}

type GRPCConfig struct {
	Port    string        `env:"GRPC_PORT" yaml:"port" env-required:"true"`
	Timeout time.Duration `env:"GRPC_TIMEOUT" yaml:"timeout" env-required:"true"`
}

func NewYAML() *Config {
	cfg := Config{}
	path := os.Getenv("CONFIG_PATH")
	if path == "" {
		panic("can not find file")
	}
	if err := cleanenv.ReadConfig(path, &cfg); err != nil {
		panic("failed read config file: " + err.Error())
	}
	return &cfg
}

func NewENV() *Config {
	cfg := Config{}
	if err := cleanenv.ReadEnv(&cfg); err != nil {
		panic("failed read envs from environment variables: " + err.Error())
	}
	return &cfg
}
