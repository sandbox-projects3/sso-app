export APP_NAME="sso-app"
export LOG_LEVEL="local"
export STORAGE_PATH="./storage/sso.db"
export TOKEN_TTL="1h"
export GRPC_PORT="8443"
export GRPC_TIMEOUT="10h"

go run ./cmd/sso